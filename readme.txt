 159.710 Assignment 1 Result Processing System implementation
 
 
 Few tips and bugs:
 
 1. Starting page is index.html.
 
 2. It does not work well with Internet Explorer.
 
 3. Bar chart will show after user set up guideline.
 
 4. All the range input must follow "number dash number" format(eg:30-50)
 
 5. I did not implement the scroll bar which can visually modified the cutting marks. The only way to do that is to change exiting cutting marks at top of index page. 
 
 6. Since there is no database, I did not consider the data persistence. That means once user apply the guideline( by click guideline name), it will transfer to index page and lost guideline information if user went back to guideline page.