

  $(function() {
	var parm=[];
	var GPASVG;
	var PieSVG;
	var gpaChart;
	var barChart;
	var barSVG;
	var rangeArray=[]
	var allStudents=[ {name:"Tom", value:89, grade:"A+"},
				{name:"Shelly", value:40, grade:"D"},
				{name:"Kim", value:60, grade:"B-"},
				{name:"April", value:76, grade:"A-"},
				{name:"Dan", value:76, grade:"A-"},
				{name:"John", value:77, grade:"A-"},
				{name:"Smith", value:92, grade:"A+"},
				{name:"Chris", value:64, grade:"B-"},
				{name:"Lee", value:79, grade:"A-"},
				{name:"Amy", value:54, grade:"C"},
				{name:"Bella", value:71, grade:"B+"},
				{name:"George", value:69, grade:"B"}				
	];

	var internal=[{name:"Tom", value:89, grade:"A+"},
				{name:"Shelly", value:40, grade:"D"},
				{name:"Kim", value:60, grade:"B-"},
				{name:"April", value:76, grade:"A-"},
				{name:"Dan", value:76, grade:"A-"},
				{name:"John", value:77, grade:"A-"},
				{name:"Smith", value:92, grade:"A+"},
				{name:"Chris", value:64, grade:"B-"}];
	var distance=[{name:"Lee", value:79, grade:"A-"},
				{name:"Amy", value:54, grade:"C"},
				{name:"Bella", value:71, grade:"B+"},
				{name:"George", value:69, grade:"B"}];

	var pieDataAll=[
		{label:"A+", color:"#088A29",value:2},
		{label:"A", color:"#81F781",value:0},
		{label:"A-", color:"#00FF40",value:4},
		{label:"B+", color:"#240B3B",value:1},
		{label:"B", color:"#4B088A",value:1},
		{label:"B-", color:"#A901DB",value:2},
		{label:"C+", color:"#0404B4",value:0},
		{label:"C", color:"#2E2EFE",value:1},
		{label:"D", color:"#8A0829",value:1},
		{label:"E", color:"#FE2E64",value:0},
	];

	var pieDataInternal=[
		{label:"A+", color:"#088A29",value:2},
		{label:"A", color:"#81F781",value:0},
		{label:"A-", color:"#00FF40",value:3},
		{label:"B+", color:"#240B3B",value:0},
		{label:"B", color:"#4B088A",value:0},
		{label:"B-", color:"#A901DB",value:2},
		{label:"C+", color:"#0404B4",value:0},
		{label:"C", color:"#2E2EFE",value:0},
		{label:"D", color:"#8A0829",value:1},
		{label:"E", color:"#FE2E64",value:0},
	];

	var pieDataDistance=[
		{label:"A+", color:"#088A29",value:0},
		{label:"A", color:"#81F781",value:0},
		{label:"A-", color:"#00FF40",value:1},
		{label:"B+", color:"#240B3B",value:1},
		{label:"B", color:"#4B088A",value:1}, 
		{label:"B-", color:"#A901DB",value:0},
		{label:"C+", color:"#0404B4",value:0},
		{label:"C", color:"#2E2EFE",value:1},
		{label:"D", color:"#8A0829",value:0},
		{label:"E", color:"#FE2E64",value:0},
	];
	
	var gpa=[{"title":"GPA","subtitle":"0-9","ranges":[0,9,9],"measures":[0,0],"markers":[5.4]}];
	var bar=[{"title":"A+","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[2]},
			{"title":"A","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[0]},
			{"title":"A-","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[4]},
			{"title":"B+","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[1]},
			{"title":"B","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[1]},
			{"title":"B-","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[2]},
			{"title":"C+","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[0]},
			{"title":"C","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[1]},
			{"title":"D","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[1]},
			{"title":"E","subtitle":"students","ranges":[0,10,10],"measures":[0,0],"markers":[0]}];
	
	var cuttingMarks=[{label:"A+",max:100,min:85},
					{label:"A", max:85,min:80},
					{label:"A-",max:80,min:75},
					{label:"B+",max:75,min:70},
					{label:"B",max:70,min:65}, 
					{label:"B-",max:65,min:60},
					{label:"C+",max:60,min:55},
					{label:"C",max:55,min:50},
					{label:"D",max:50,min:30},
					{label:"E",max:30,min:0}];		
	var barGuideline=[];

	$( "#waring" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
	displayCuttingMarks();
	displayStudents(allStudents);
	initPieChart();
	initGPAGraph();
	initBarChart(bar);

	settingMarks(allStudents,pieDataAll);	
	$("#all").on("click",function(){
		displayStudents(allStudents);
		changePieChart(pieDataAll);	
		settingMarks(allStudents,pieDataAll);
		$("#studentGroupDisplay").text("All Students");
	});
	
	$("#internal").on("click",function(){
		displayStudents(internal);
		changePieChart(pieDataInternal);
		settingMarks(internal,pieDataInternal);
		$("#studentGroupDisplay").text("Internal Students");
	});
	$("#distance").on("click",function(){
		displayStudents(distance);
		changePieChart(pieDataDistance);
		settingMarks(distance,pieDataDistance);
		$("#studentGroupDisplay").text("Distance Students");
	});
	
	
	if(window.location.search!=""){
		parm=getUrlVars();
		var range=parm.range;
		var grade=parm.grade;
		var count=parm.count;
		var gpaRange=parm.gpa;
		var from;
		var to;
		var total=0;
		for(var i=0;i<count;i++){
			var desired=parseInt(grade.split('-')[i]);
			total=pieDataAll[desired].value+total;
			rangeArray.push(desired);
			$("#colored th:nth-child("+(desired+1)+")").css("background-color", "yellow");
		}
		
		for(var i=0;i< bar.length;i++){
			if(rangeArray[0]==i){
				from=bar[i].title;
			}
			if(rangeArray[rangeArray.length-1]==i){
				to=bar[i].title;
			}
			if(jQuery.inArray( i, rangeArray ) == -1 ){
				barGuideline.push(bar[i]);
			}
		}

		barGuideline.push({"title":""+from+" to "+to+"","subtitle":"students","ranges":[0,10,10],"measures":[parseInt(range.split('-')[0])/100*12,parseInt(range.split('-')[1])/100*12],"markers":[total]});
		$("#barRemove").remove();
		initBarChart(barGuideline);	

		if(gpaRange != ""){
			gpa[0].measures[0]= parseInt(gpaRange.split('-')[0]);
			gpa[0].measures[1]=parseInt(gpaRange.split('-')[1]);
			GPASVG.data(gpa).call(gpaChart.duration(1000));
			
		}
	}
	
	
	
	 function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}
	function displayCuttingMarks(){
		$("tr#bindkeyup td input").each(function(){
			var column_num = parseInt( $( this ).parent().index() );
			$( this ).val(cuttingMarks[column_num].max.toString()+"-"+cuttingMarks[column_num].min.toString());
		});
	}
	function displayStudents(marks){
		$("tr.student").each(function(){
			$(this).remove();
		});
		for(var i=0;i<marks.length;i++){
			$("#appendMarks").append("<tr class='student'><td>"+marks[i].name+"</td><td>"+marks[i].value+"</td><td>"+marks[i].grade+"</td></tr>");
		}
	}
  
	function initPieChart(){
		PieSVG = d3.select("#piechart").append("svg").attr("width", 350).attr("height", 320);
		PieSVG.append("g").attr("id","quotesDonut");
		Donut3D.draw("quotesDonut", pieDataAll, 200, 150, 150, 125, 30, 0);
	}
	
	function initBarChart( input ){
		var margin = {top: 0, right: 20, bottom: 20, left: 240},
			width = 960 - margin.left - margin.right,
			height = 50 - margin.top - margin.bottom;

		barChart = d3.bullet()
			.width(width)
			.height(height);
		$("#barchart").append("<div id='barRemove'></div>")
		barSVG = d3.select("#barRemove").selectAll("svg")
					.data(input).enter().append("svg")
					.attr("class", "bullet")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
					.call(barChart);
	  var title = barSVG.append("g")
		  .style("text-anchor", "end")
		  .attr("transform", "translate(-6," + height / 2 + ")");

	  title.append("text")
		  .attr("class", "title")
		  .text(function(d) { return d.title; });

	  title.append("text")
		  .attr("class", "subtitle")
		  .attr("dy", "1em")
		  .text(function(d) { return d.subtitle; });		
	}
	
	function changePieChart(piedata){
		Donut3D.transition("quotesDonut",piedata, 150, 125, 30, 0);
	}
	function initGPAGraph(){
		var margin = {top: 0, right: 20, bottom: 20, left: 240},
			width = 960 - margin.left - margin.right,
			height = 50 - margin.top - margin.bottom;

		gpaChart = d3.bullet()
			.width(width)
			.height(height);
		GPASVG = d3.select("#gpaGraph").selectAll("svg")
					.data(gpa).enter().append("svg")
					.attr("class", "bullet")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
					.call(gpaChart);
	  var title = GPASVG.append("g")
		  .style("text-anchor", "end")
		  .attr("transform", "translate(-6," + height / 2 + ")");

	  title.append("text")
		  .attr("class", "title")
		  .text(function(d) { return d.title; });

	  title.append("text")
		  .attr("class", "subtitle")
		  .attr("dy", "1em")
		  .text(function(d) { return d.subtitle; });
		}

	function generatepGPA(marks){
		var count=0;
	
			for(var i=0;i<12;i++){
				if(marks[i].grade=="A+"){count=count+9;}
				if(marks[i].grade=="A"){count=count+8;}
				if(marks[i].grade=="A-"){count=count+7;}
				if(marks[i].grade=="B+"){count=count+6;}
				if(marks[i].grade=="B"){count=count+5;}
				if(marks[i].grade=="B-"){count=count+4;}
				if(marks[i].grade=="C+"){count=count+3;}
				if(marks[i].grade=="C"){count=count+2;}
				if(marks[i].grade=="E"){count=count+0;}
				if(marks[i].grade=="D"){count=count+0;}
				
			}
			gpa[0].markers=[count/12];
			GPASVG.data(gpa).call(gpaChart.duration(1000));	
	}
	function settingMarks(marks,pieData){
		$("tr#bindkeyup td input").each(function(){
		var $thisCol = $( this );
		var column_num = parseInt( $thisCol.parent().index() );
		$thisCol.bind("change", function(){
				if($thisCol.val() != ""){
					var max=parseInt($thisCol.val().split('-')[0]);
					var min=parseInt($thisCol.val().split('-')[1]);
					console.log(column_num);
					if(column_num == 0){
						cuttingMarks[column_num+1].max=min;
					}else if(column_num == 9){
						cuttingMarks[column_num-1].min=max;
					}else{
						cuttingMarks[column_num-1].min=max;
						cuttingMarks[column_num+1].max=min;
					}
					
					cuttingMarks[column_num].min=min;
					cuttingMarks[column_num].max=max;
					displayCuttingMarks();
					
					for(var i=0;i<cuttingMarks.length;i++){
						var count=0;
						for(var j=0;j<12;j++){
							if(marks[j].value>=cuttingMarks[i].min && marks[j].value<cuttingMarks[i].max){
								count++;
								marks[j].grade=pieData[i].label;
								displayStudents(marks);
								generatepGPA(marks);
							}
						}
					}
					

					 
					var total=0;
					pieData[column_num].value=count;
					Donut3D.transition("quotesDonut",pieData, 150, 125, 30, 0);
					for(var i=0;i<rangeArray.length;i++){
						total=total+pieData[rangeArray[i]].value;
					}
					
					
					if(barGuideline.length !=0){
						for(var i=0;i<barGuideline.length;i++){
							if(barGuideline[i].title==pieData[column_num].label){
								barGuideline[i].markers=[pieData[column_num].value];
							}
						}
						barGuideline[barGuideline.length-1].markers=[total];
						barSVG.data(barGuideline).call(barChart.duration(1000));
					}else{
						for(var i=0;i<bar.length;i++){
							if(bar[i].title==pieData[column_num].label){
								bar[i].markers=[pieData[column_num].value];
							}
						
						}
						bar[bar.length-1].markers=[total];
						barSVG.data(bar).call(barChart.duration(1000));
					}	
					
				}
			});
		});
	}
	
  });
  
 !function(){
	var Donut3D={};
	
	function pieTop(d, rx, ry, ir ){
		if(d.endAngle - d.startAngle == 0 ) return "M 0 0";
		var sx = rx*Math.cos(d.startAngle),
			sy = ry*Math.sin(d.startAngle),
			ex = rx*Math.cos(d.endAngle),
			ey = ry*Math.sin(d.endAngle);
			
		var ret =[];
		ret.push("M",sx,sy,"A",rx,ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0),"1",ex,ey,"L",ir*ex,ir*ey);
		ret.push("A",ir*rx,ir*ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0), "0",ir*sx,ir*sy,"z");
		return ret.join(" ");
	}

	function pieOuter(d, rx, ry, h ){
		var startAngle = (d.startAngle > Math.PI ? Math.PI : d.startAngle);
		var endAngle = (d.endAngle > Math.PI ? Math.PI : d.endAngle);
		
		var sx = rx*Math.cos(startAngle),
			sy = ry*Math.sin(startAngle),
			ex = rx*Math.cos(endAngle),
			ey = ry*Math.sin(endAngle);
			
			var ret =[];
			ret.push("M",sx,h+sy,"A",rx,ry,"0 0 1",ex,h+ey,"L",ex,ey,"A",rx,ry,"0 0 0",sx,sy,"z");
			return ret.join(" ");
	}

	function pieInner(d, rx, ry, h, ir ){
		var startAngle = (d.startAngle < Math.PI ? Math.PI : d.startAngle);
		var endAngle = (d.endAngle < Math.PI ? Math.PI : d.endAngle);
		
		var sx = ir*rx*Math.cos(startAngle),
			sy = ir*ry*Math.sin(startAngle),
			ex = ir*rx*Math.cos(endAngle),
			ey = ir*ry*Math.sin(endAngle);

			var ret =[];
			ret.push("M",sx, sy,"A",ir*rx,ir*ry,"0 0 1",ex,ey, "L",ex,h+ey,"A",ir*rx, ir*ry,"0 0 0",sx,h+sy,"z");
			return ret.join(" ");
	}

	function getPercent(d){
		return (d.endAngle-d.startAngle > 0.2 ? 
				d.data.label+':'+Math.round(1000*(d.endAngle-d.startAngle)/(Math.PI*2))/10+'%' : '');
	}	
	
	Donut3D.transition = function(id, data, rx, ry, h, ir){
		function arcTweenInner(a) {
		  var i = d3.interpolate(this._current, a);
		  this._current = i(0);
		  return function(t) { return pieInner(i(t), rx+0.5, ry+0.5, h, ir);  };
		}
		function arcTweenTop(a) {
		  var i = d3.interpolate(this._current, a);
		  this._current = i(0);
		  return function(t) { return pieTop(i(t), rx, ry, ir);  };
		}
		function arcTweenOuter(a) {
		  var i = d3.interpolate(this._current, a);
		  this._current = i(0);
		  return function(t) { return pieOuter(i(t), rx-.5, ry-.5, h);  };
		}
		function textTweenX(a) {
		  var i = d3.interpolate(this._current, a);
		  this._current = i(0);
		  return function(t) { return 0.6*rx*Math.cos(0.5*(i(t).startAngle+i(t).endAngle));  };
		}
		function textTweenY(a) {
		  var i = d3.interpolate(this._current, a);
		  this._current = i(0);
		  return function(t) { return 0.6*rx*Math.sin(0.5*(i(t).startAngle+i(t).endAngle));  };
		}
		
		var _data = d3.layout.pie().sort(null).value(function(d) {return d.value;})(data);
		
		d3.select("#"+id).selectAll(".innerSlice").data(_data)
			.transition().duration(750).attrTween("d", arcTweenInner); 
			
		d3.select("#"+id).selectAll(".topSlice").data(_data)
			.transition().duration(750).attrTween("d", arcTweenTop); 
			
		d3.select("#"+id).selectAll(".outerSlice").data(_data)
			.transition().duration(750).attrTween("d", arcTweenOuter); 	
			
		d3.select("#"+id).selectAll(".percent").data(_data).transition().duration(750)
			.attrTween("x",textTweenX).attrTween("y",textTweenY).text(getPercent); 	
	}
	
	Donut3D.draw=function(id, data, x /*center x*/, y/*center y*/, 
			rx/*radius x*/, ry/*radius y*/, h/*height*/, ir/*inner radius*/){
	
		var _data = d3.layout.pie().sort(null).value(function(d) {return d.value;})(data);
		
		var slices = d3.select("#"+id).append("g").attr("transform", "translate(" + x + "," + y + ")")
			.attr("class", "slices");
			
		slices.selectAll(".innerSlice").data(_data).enter().append("path").attr("class", "innerSlice")
			.style("fill", function(d) { return d3.hsl(d.data.color).darker(0.7); })
			.attr("d",function(d){ return pieInner(d, rx+0.5,ry+0.5, h, ir);})
			.each(function(d){this._current=d;});
		
		slices.selectAll(".topSlice").data(_data).enter().append("path").attr("class", "topSlice")
			.style("fill", function(d) { return d.data.color; })
			.style("stroke", function(d) { return d.data.color; })
			.attr("d",function(d){ return pieTop(d, rx, ry, ir);})
			.each(function(d){this._current=d;});
		
		slices.selectAll(".outerSlice").data(_data).enter().append("path").attr("class", "outerSlice")
			.style("fill", function(d) { return d3.hsl(d.data.color).darker(0.7); })
			.attr("d",function(d){ return pieOuter(d, rx-.5,ry-.5, h);})
			.each(function(d){this._current=d;});

		slices.selectAll(".percent").data(_data).enter().append("text").attr("class", "percent")
			.attr("x",function(d){ return 0.6*rx*Math.cos(0.5*(d.startAngle+d.endAngle));})
			.attr("y",function(d){ return 0.6*ry*Math.sin(0.5*(d.startAngle+d.endAngle));})
			.text(getPercent).each(function(d){this._current=d;});				
	}
	
	this.Donut3D = Donut3D;
}();
(function() {

// Chart design based on the recommendations of Stephen Few. Implementation
// based on the work of Clint Ivy, Jamie Love, and Jason Davies.
// http://projects.instantcognition.com/protovis/bulletchart/
d3.bullet = function() {
  var orient = "left", // TODO top & bottom
      reverse = false,
      duration = 0,
      ranges = bulletRanges,
      markers = bulletMarkers,
      measures = bulletMeasures,
      width = 380,
      height = 30,
      tickFormat = null;

  // For each small multiple�
  function bullet(g) {
    g.each(function(d, i) {
      var rangez = ranges.call(this, d, i).slice().sort(d3.descending),
          markerz = markers.call(this, d, i).slice().sort(d3.descending),
          measurez = measures.call(this, d, i).slice().sort(d3.descending),
          g = d3.select(this);

      // Compute the new x-scale.
      var x1 = d3.scale.linear()
          .domain([0, Math.max(rangez[0], markerz[0], measurez[0])])
          .range(reverse ? [width, 0] : [0, width]);

      // Retrieve the old x-scale, if this is an update.
      var x0 = this.__chart__ || d3.scale.linear()
          .domain([0, Infinity])
          .range(x1.range());

      // Stash the new scale.
      this.__chart__ = x1;

      // Derive width-scales from the x-scales.
      var w0 = bulletWidth(x0),
          w1 = bulletWidth(x1);

      // Update the range rects.
      var range = g.selectAll("rect.range")
          .data(rangez);

      range.enter().append("rect")
          .attr("class", function(d, i) { return "range s" + i; })
          .attr("width", w0)
          .attr("height", height)
          .attr("x", reverse ? x0 : 0)
        .transition()
          .duration(duration)
          .attr("width", w1)
          .attr("x", reverse ? x1 : 0);

      range.transition()
          .duration(duration)
          .attr("x", reverse ? x1 : 0)
          .attr("width", w1)
          .attr("height", height);

      // Update the measure rects.
      var measure = g.selectAll("rect.measure")
          .data(measurez);

      measure.enter().append("rect")
          .attr("class", function(d, i) { return "measure s" + i; })
          .attr("width", w0)
          .attr("height", height / 3)
          .attr("x", reverse ? x0 : 0)
          .attr("y", height / 3)
        .transition()
          .duration(duration)
          .attr("width", w1)
          .attr("x", reverse ? x1 : 0);

      measure.transition()
          .duration(duration)
          .attr("width", w1)
          .attr("height", height / 3)
          .attr("x", reverse ? x1 : 0)
          .attr("y", height / 3);

      // Update the marker lines.
      var marker = g.selectAll("line.marker")
          .data(markerz);

      marker.enter().append("line")
          .attr("class", "marker")
          .attr("x1", x0)
          .attr("x2", x0)
          .attr("y1", height / 6)
          .attr("y2", height * 5 / 6)
        .transition()
          .duration(duration)
          .attr("x1", x1)
          .attr("x2", x1);

      marker.transition()
          .duration(duration)
          .attr("x1", x1)
          .attr("x2", x1)
          .attr("y1", height / 6)
          .attr("y2", height * 5 / 6);

      // Compute the tick format.
      var format = tickFormat || x1.tickFormat(8);

      // Update the tick groups.
      var tick = g.selectAll("g.tick")
          .data(x1.ticks(8), function(d) {
            return this.textContent || format(d);
          });

      // Initialize the ticks with the old scale, x0.
      var tickEnter = tick.enter().append("g")
          .attr("class", "tick")
          .attr("transform", bulletTranslate(x0))
          .style("opacity", 1e-6);

      tickEnter.append("line")
          .attr("y1", height)
          .attr("y2", height * 7 / 6);

      tickEnter.append("text")
          .attr("text-anchor", "middle")
          .attr("dy", "1em")
          .attr("y", height * 7 / 6)
          .text(format);

      // Transition the entering ticks to the new scale, x1.
      tickEnter.transition()
          .duration(duration)
          .attr("transform", bulletTranslate(x1))
          .style("opacity", 1);

      // Transition the updating ticks to the new scale, x1.
      var tickUpdate = tick.transition()
          .duration(duration)
          .attr("transform", bulletTranslate(x1))
          .style("opacity", 1);

      tickUpdate.select("line")
          .attr("y1", height)
          .attr("y2", height * 7 / 6);

      tickUpdate.select("text")
          .attr("y", height * 7 / 6);

      // Transition the exiting ticks to the new scale, x1.
      tick.exit().transition()
          .duration(duration)
          .attr("transform", bulletTranslate(x1))
          .style("opacity", 1e-6)
          .remove();
    });
    d3.timer.flush();
  }

  // left, right, top, bottom
  bullet.orient = function(x) {
    if (!arguments.length) return orient;
    orient = x;
    reverse = orient == "right" || orient == "bottom";
    return bullet;
  };

  // ranges (bad, satisfactory, good)
  bullet.ranges = function(x) {
    if (!arguments.length) return ranges;
    ranges = x;
    return bullet;
  };

  // markers (previous, goal)
  bullet.markers = function(x) {
    if (!arguments.length) return markers;
    markers = x;
    return bullet;
  };

  // measures (actual, forecast)
  bullet.measures = function(x) {
    if (!arguments.length) return measures;
    measures = x;
    return bullet;
  };

  bullet.width = function(x) {
    if (!arguments.length) return width;
    width = x;
    return bullet;
  };

  bullet.height = function(x) {
    if (!arguments.length) return height;
    height = x;
    return bullet;
  };

  bullet.tickFormat = function(x) {
    if (!arguments.length) return tickFormat;
    tickFormat = x;
    return bullet;
  };

  bullet.duration = function(x) {
    if (!arguments.length) return duration;
    duration = x;
    return bullet;
  };

  return bullet;
};

function bulletRanges(d) {
  return d.ranges;
}

function bulletMarkers(d) {
  return d.markers;
}

function bulletMeasures(d) {
  return d.measures;
}

function bulletTranslate(x) {
  return function(d) {
    return "translate(" + x(d) + ",0)";
  };
}

function bulletWidth(x) {
  var x0 = x(0);
  return function(d) {
    return Math.abs(x(d) - x0);
  };
}

})();  
  