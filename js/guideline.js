  $(function() {
    var dialogNew, form,listGrades=[false,false,false,false,false,false,false,false,false,false],
      name = $( "#name" ),
      gpa =$('#gpa'),
      range=$('#range'),

      allFields = $( [] ).add( name ).add(gpa).add(range);

 
    function addGuideline() {
      var grade="";
	  var count=0;
      jQuery.each( listGrades, function(i,val) {
          if(val == true){
            grade=grade+i+"-";
			count++;
          }
      });

      $( "#users tbody" ).append( "<tr>" +
        "<td><a href='index.html?name="+name.val()+"&&range="+range.val()+"&&grade="+grade+"&&gpa="+gpa.val()+"&&count="+count+"'>" 
        + name.val()+ "</a></td>" +
		    "<td class='btnDelete'>Delete</td>"+
			"<td class='apply'><a href='index.html?name="+name.val()+"&&range="+range.val()+"&&grade="+grade+"&&gpa="+gpa.val()+"&&count="+count+"'>Apply</a></td>"+
        "</tr>" );
	   $(".btnDelete").bind("click", function(){
		    $(this).parent().remove();
	   });
	   dialogNew.dialog("close");
    
    }

  function reset(){
      jQuery.each( listGrades, function(i,val) {
          val=false;
      });
      $("tr#select td").each(function(){
          $(this).css("background-color", "white");
      });
  }
 
    dialogNew = $( "#dialog-form-new" ).dialog({
      autoOpen: false,
      height:370,
      width: 600,
      modal: true,
      buttons: {
        "Save": addGuideline,
        Cancel: function() {
          dialogNew.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        reset();
      }
    });	
 
    form = dialogNew.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addGuideline();
    });
 
    $( "#createGuidelineButton" ).on( "click", function() {
      dialogNew.dialog( "open" );
 
    });

  $("tr#select td").each(function(){
    var $thisCol = $( this );
    var count = 0;
    
    $thisCol.click(function() {
      var column_num = parseInt( $(this).index() );
      
      count++;
      
      if(count % 2 == 0){ 
        listGrades[column_num]=false;        
      }else{
        listGrades[column_num]=true;
      }

      if(listGrades[column_num] == true ){
         $thisCol.css("background-color", "yellow");
      }else{
         $thisCol.css("background-color", "white");
      }
    });
  })

  });